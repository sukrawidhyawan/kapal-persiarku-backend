# kapalpesiarku.com API (Golang with Echo)
An API for website https://kapalpesiarku.com

link Echo Framework:  [click here](https://echo.labstack.com/)

### Install

- clone the repository.
- run `dep ensure -v --vendor-only` to install the dependencies.


### Run
go run main.go